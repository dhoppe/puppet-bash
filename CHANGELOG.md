## 2015-09-03 Release 1.0.7
### Summary:
- [Puppet] Allow the parameter hostname_prompt to be configured for the user root as well

## 2015-09-03 Release 1.0.6
### Summary:
- [Puppet] Allow the parameter hostname_prompt to be configured

## 2015-08-13 Release 1.0.5
### Summary:
- [Beaker] Update Beaker environment
- [RSpec] Update RSpec environment
- [Travis CI] Update Travis CI environment
- [Puppet Forge] Update license, version requirement

## 2015-02-25 Release 1.0.4
### Summary:
- [Beaker] Update Beaker environment
- [Travis CI] Update Travis CI environment

## 2015-02-25 Release 1.0.3
### Summary:
- [Beaker] Update Beaker environment
- [Puppet] Add support for Debian 8.x (Jessie)

## 2014-12-06 Release 1.0.2
### Summary:
- [Rspec] Made some changes to the build environment

## 2014-11-28 Release 1.0.1
### Summary:
- [Beaker] Remove test

## 2014-11-27 Release 1.0.0
### Summary:
- Generated from [https://github.com/dhoppe/puppet-skeleton-simple](https://github.com/dhoppe/puppet-skeleton-simple)
